#!/bin/bash

data_atual=$(date +"%d/%m/%Y")
prox_quarta=$(date -d "next wednesday" +"%d/%m/%Y")

echo =====================================
echo "Data atual"

echo "Data atual: $data_atual"
echo =========================================
echo "Data das próximas quartas-feiras"
echo ===========================================
echo "Próxima quarta-feira: $prox_quarta"
echo "Quarta-feira seguinte: $(date -d "next wednesday +7 days" +"%d/%m/%Y")"
echo "Quarta-feira seguinte: $(date -d "next wednesday +14 days" +"%d/%m/%Y")"
echo "Quarta-feira seguinte: $(date -d "next wednesday +21 days" +"%d/%m/%Y")"

echo =========================================================
echo  "Diretórios com os nomes backup"
mkdir "backup$data_atual"
mkdir "backup$prox_quarta"
mkdir "backup$(date -d "next wednesday +7 days" +"%d/%m/%Y")"
mkdir "backup$(date -d "next wednesday +14 days" +"%d/%m/%Y")"

